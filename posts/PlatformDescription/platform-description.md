# Dev Blog Platform .getDescription
 
> That's &#128070; really the title, not an error &#128556;


## Background
Every developer reaches a point in his/her life when the desire to build something from scratch with the help of the experience one has gathered until that moment, reveals itself. 

Being so passionate about all these geek programming stuff and the fact that I realise some things: "*oh! I know how to do that*" and "*I have some experience with this*" and the most important one **"how hard can it be?"** got me started to make a plan on how can I combine all of these into one. 

### The plan
I wanted to apply all of the good practices I've learnt (and still learning) from my job and by myself, both technical and non-technical. 

For the technical part we can include: 

 - a good architectural pattern that will allow me to use the newest (and **mature**) technologies
 - implementing security around the platform
 - CI/CD was also a must
 - a git platform for code versioning

For the non-technical part:

 - the first thing on the list was an app that would allow me to mimic the JIRA board where I can keep my tasks, so I can be as organised as possible (and free), therefore -  [ClickUp](https://clickup.com/onboarding?fp_ref=48cb1)
 - do I need analytics ?  - of course you do, you always do
 - Privacy Policy & Cookie Policy 'coz GDPR
 - how much do i spent on hosting? (ideally, nothing &#128556;)
 - *what will the platform be about?* - the true golden question

### What shall I build?
Of course I had no idea, but being the first app that I will release to the public, I wanted to make sure I get the hang on how things are with the process. I wanted an app that kept *no sensitive data*, to reduce security risk, and will be somehow *useful* for both me and others.

I thought that a presentation platform would be perfect. A place where I can "*sell*" myself better than I do on LinkedIn or other social platforms, and about the "*and others*" part....&#129300; why not a blog? I do have a curious personality and often I come across things that *stackoverflow* or googling it doesn't solve them...Ok, maybe they do, but not up 100%. Those few extra miles, that I had to do alone, thought me so much that every time I have the opportunity, I share it with others - a (dev) blog it is then.


## Architecture
I won't go too much into details here, that's a topic for another post, but, this is the architecture seen from above:

![enter image description here](https://drive.google.com/uc?export=download&id=186fgQ4a-cuo2f3und5fd5YUkR1pFTCBM)
### Frontend
There are 2 services for frontend: One for administration purposes, creation of posts, messages etc, and the one that you are using right now - let's call it the client. Both of them are SPA ( single page applications) done with **Angular 9**. The admin dashboard is hosted on [Heroku](https://www.heroku.com) - I heard about it before and wanted to explore it, plus it would've prevented me to start another instance to host it on the kubernetes cluster => cheaper for me. 

The client is hosted on a kubernetes cluster on google cloud.

### Backend
For backend I created 2 services: One that handles authentication of the requests and also manages users and the other one that feeds data for posts and holds messages sent from the contact form. Both of them were developed using **java** with **spring boot** . 

Both of them are connected to [Mongodb Atlas](https://www.mongodb.com/cloud/atlas) - it creates a cluster of mongo dbs so I didn't need to worry about spinning instances for databases.

### Infrastructure
As you probably guessed from what I've said above, most of the platform parts are deployed on a **kubernetes** cluster. I've used a NGINX Ingress Controller as an ApiGateway, in front of which is a *Service* of type `LoadBalancer` which is given a *public ip* . Services for pods also act as LoadBalancers for the requests that arrive on certain paths. 

### CI/CD
As a software engineer showing diligence, I am using git as a version control tool, together with [Gitlab](https://gitlab.com). Every component has its own repository with a CI/CD pipeline script. So the flow goes like this:

 1. I think of a feature/task/defect 
 2. I create the ticket to the corresponding *ClickUp Board* - ClickUp is connected with my GitlabAccount so if i push commits which start with a certain id: e.g. `#4k5n5s Test message` those respective commits and branches are picked up by clickup - ![enter image description here](https://drive.google.com/uc?export=download&id=1ENDhE_csIA85ihvw3Bvwo-rZ4zXVx-su)
 3. I create a branch on which I push commits as described above
 4. Open MR (merge requests - and of course I self approve them | this is just a personal choice to keep myself organised)
 5. If the CI passed - CI contains **build + tests** (haven't written many though &#128556;) - I merge the branch into develop
 6. The pipeline on develop starts - this pipeline contains also CI steps from above but also CD steps: **publishAssets + deployAssets**
	 
	 - I build and publish the images of the apps on a private repo from dockerhub which are then used when the pods are created
	 - The deploy step just updates a date on the deployment on the cluster which pulls the image again
 
So every time I'm merging a short-lived branch into *develop* (which is my main branch), a new version of the app gets deployed, replacing the old one

### Security
I've set rules inside NGINX Ingress Controller to limit access to resources in order to be protected against a DDoS attacks. Also, calls to backend are limited by user roles held inside a JWT, which is obviously encrypted and doesn't contain any sensitive informations like password.

## Blog
Here I want to post articles in which I can share my thoughts about interesting things I came across developing. I'll try to  keep them as captivating as possible. Please feel free to add comments with feedback, questions or anything you want :)

## Conclusion
I hope you found this first post interesting to some extent. It's not as technical as I would've liked, but it was more an introduction. The next ones will definitely have some code in them ! 
